/**
 * Module dependencies.
 */
ApiClient = require('../index.js').ApiClient
const express = require('express')
const app = express()
var cors = require('cors')
app.use(cors())

const client = new ApiClient({
  appkey: '32820017',
  appsecret: '9a9d74d679c81c81448135093cf36041',
  url: 'http://gw.api.taobao.com/router/rest',
})

app.get('/categories', (req, res) => {
  client.executeWithHeader(
    'aliexpress.affiliate.category.get',
    {},
    {},
    function (error, response) {
      if (!error) res.send(response)
      else res.send(error)
    },
  )
})

app.get('/products', (req, res) => {
  if (req.query.category_ids) {
    client.executeWithHeader(
      'aliexpress.affiliate.product.query',
      {
        page_no: req.query.page_no,
        category_ids: req.query.category_ids,
        page_size: req.query.page_size,
        min_sale_price: req.query.min_sale_price,
        sort: req.query.sort,
        fields:
          'app_sale_price,original_price,discount,latest_volume,promotion_link,product_id,product_main_image_url,product_small_image_urls,product_title,second_level_category_name,first_level_category_name',
      },
      {},
      function (error, response) {
        if (!error) res.send(response)
        else res.send(error)
      },
    )
  }

  if (req.query.keywords) {
    client.executeWithHeader(
      'aliexpress.affiliate.product.query',
      {
        page_no: req.query.page_no,
        keywords: req.query.keywords,
        page_size: req.query.page_size,
        min_sale_price: req.query.min_sale_price,
        sort: req.query.sort,
        fields:
          'app_sale_price,original_price,discount,latest_volume,promotion_link,product_id,product_main_image_url,product_small_image_urls,product_title,second_level_category_name,first_level_category_name',
      },
      {},
      function (error, response) {
        if (!error) res.send(response)
        else res.send(error)
      },
    )
  }
})

app.get('/hotproducts', (req, res) => {
  if (req.query.category_ids) {
    client.executeWithHeader(
      'aliexpress.affiliate.hotproduct.query',
      {
        page_no: req.query.page_no,
        category_ids: req.query.category_ids,
        page_size: req.query.page_size,
        min_sale_price: req.query.min_sale_price,
        sort: req.query.sort,
        fields:
          'app_sale_price,original_price,discount,latest_volume,promotion_link,product_id,product_main_image_url,product_small_image_urls,product_title,second_level_category_name,first_level_category_name',
      },
      {},
      function (error, response) {
        if (!error) res.send(response)
        else res.send(error)
      },
    )
  }

  if (req.query.keywords) {
    client.executeWithHeader(
      'aliexpress.affiliate.hotproduct.query',
      {
        page_no: req.query.page_no,
        keywords: req.query.keywords,
        page_size: req.query.page_size,
        min_sale_price: req.query.min_sale_price,
        sort: req.query.sort,
        fields:
          'app_sale_price,original_price,discount,latest_volume,promotion_link,product_id,product_main_image_url,product_small_image_urls,product_title,second_level_category_name,first_level_category_name',
      },
      {},
      function (error, response) {
        if (!error) res.send(response)
        else res.send(error)
      },
    )
  }
})

app.get('/featuredproducts', (req, res) => {
  client.executeWithHeader(
    'aliexpress.affiliate.featuredpromo.products.get',
    {
      page_no: req.query.page_no,
      // keywords: req.query.keywords,
      page_size: req.query.page_size,
      min_sale_price: req.query.min_sale_price,
      sort: req.query.sort,
      promotion_name: req.query.promotion_name,
    },
    {},
    function (error, response) {
      if (!error) res.send(response)
      else res.send(error)
    },
  )
})

app.get('/promoinfo', (req, res) => {
  if (req.query.category_id) {
    client.executeWithHeader(
      'aliexpress.affiliate.featuredpromo.products.get',
      {
        promotion_name: req.query.promotion_name,
        page_no: req.query.page_no,
        page_size: req.query.page_size,
        sort: req.query.sort,
        category_id: req.query.category_id,
      },
      {},
      function (error, response) {
        if (!error) res.send(response)
        else res.send(error)
      },
    )
  }
  if (req.query.sort) {
    client.executeWithHeader(
      'aliexpress.affiliate.featuredpromo.products.get',
      {
        promotion_name: req.query.promotion_name,
        page_no: req.query.page_no,
        page_size: req.query.page_size,
        sort: req.query.sort,
      },
      {},
      function (error, response) {
        if (!error) res.send(response)
        else res.send(error)
      },
    )
  }
  if (!req.query.sort) {
    client.executeWithHeader(
      'aliexpress.affiliate.featuredpromo.products.get',
      {
        promotion_name: req.query.promotion_name,
        page_no: req.query.page_no,
        page_size: req.query.page_size,
      },
      {},
      function (error, response) {
        if (!error) res.send(response)
        else res.send(error)
      },
    )
  }
})

app.get('/konopna-products', (req, res) => {
  client.executeWithHeader(
    'aliexpress.affiliate.product.query',
    {
      page_no: req.query.page_no,
      keywords: req.query.keywords,
      page_size: req.query.page_size,
      min_sale_price: req.query.min_sale_price,
      target_currency: 'PLN',
      target_language: 'PL',
      fields:
        'app_sale_price,original_price,discount,latest_volume,promotion_link,product_id,product_main_image_url,product_small_image_urls,product_title,second_level_category_name,first_level_category_name',
    },
    {},
    function (error, response) {
      if (!error) res.send(response)
      else res.send(error)
    },
  )
})

app.get('/konopna-hot-products', (req, res) => {
  client.executeWithHeader(
    'aliexpress.affiliate.hotproduct.query',
    {
      page_no: req.query.page_no,
      keywords: req.query.keywords,
      page_size: req.query.page_size,
      min_sale_price: req.query.min_sale_price,
      target_currency: 'PLN',
      target_language: 'PL',
      fields:
        'app_sale_price,original_price,discount,latest_volume,promotion_link,product_id,product_main_image_url,product_small_image_urls,product_title,second_level_category_name,first_level_category_name',
    },
    {},
    function (error, response) {
      if (!error) res.send(response)
      else res.send(error)
    },
  )
})

app.listen(9000, () => {
  console.log(`Server is running at port 9000`)
})
